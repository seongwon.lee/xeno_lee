FROM httpd:latest
LABEL : judo0179@gmail.com

COPY . /usr/local/apache2/htdocs/
COPY ./httpd.conf /usr/local/apache2/conf/httpd.conf

EXPOSE 80 
